# Informacje ogólne

Aplikacja Skrimish służy do zapisywania oraz przeglądania historii gier w "League of Legends". Pozwala na stworzenie wielu drużyn, a do każdej z nich można dodać dowolną liczbę gier, które będą zapisywane w historii gier danej drużyny. Takie rozwiązanie pozwala na przykład na zapisywanie wyników całej ligii. 

# Opis aplikacji
### Użytkownik
* Strona główna, zawiera formularz logowania oraz dwa przyciski: jeden z nich "register", pozwala na rejestrację nowego konta, natomiast drugi "admin" przechodzi do widoku, zawierającego formularz pozwalający zalogować się adminowi.
* Po zalogowaniu głównym widokiem jest podstrona teams, na której wyświetlają się wszystkie drużyny danego użytkownika. W lewej części strony znajduję pasek nawigacyjny zawierający 3 przyciski: create team, teams oraz settings. U góry strony znajduję się search bar, które pozwala przeglądać drużyny za pomocą fetch-api na podstawie nazw oraz lig. Każdy kafelek zawierający informacje o drużynie jest przyciskiem, który wyświetla informacje o danej drużynie.
* podstrona team pojawia się po kliknięciu na kafelek z daną drużyną, zawiera ona informacje o niej oraz przycisk "match history".
* Kliknięcie przycisku "match history" przenosi użytkownika do historii gier danej drużyny.
* Strona "create team" zawiera formularz pozwalający stworzyć nową drużynę.
* Strona "settings" zawiera informacje o danym użytkowniku oraz przycisk "change password", który pozwala zmienić hasła danego konta
### Admin
* po zalogowaniu się jako admin, główna strona wyświetla informacje o wszystkich użytkownikach posiadających konta w aplikacji
# Baza danych 

### Diagram ERD
![baza](./md/erd.PNG)


# Technologie
* Html
* Css
* JavaScript
* PostgreSQL
* PHP
* nginx

# Zrzuty ekranu

### Login
![logowanie](./md/1.PNG)
### Login as admin
![logowanie_admin](./md/2.PNG)
### Register
![rejestracja](./md/3.PNG)
### Teams
![teams](./md/4.PNG)
### Team
![team](./md/5.PNG)
### History
![history](./md/6.PNG)
### Create Team
![create_team](./md/7.PNG)
### Settings
![settings](./md/8.PNG)
### Change password
![change_password](./md/9.PNG)

# Przykłady responsywności wymiary: 411x731
![res1](./md/10.PNG)
![res2](./md/11.PNG)
![res3](./md/12.PNG)
![res4](./md/13.PNG)
![res5](./md/14.PNG)
