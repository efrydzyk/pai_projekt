<?php

require 'Routing.php';



$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('', 'DefaultController');
Routing::get('teams', 'TeamController');
Routing::get('settings', 'SecurityController');

Routing::get('team', 'SpecificTeamController');
Routing::get('displayHistory', 'SpecificTeamController');
Routing::get('logout','SecurityController');
Routing::get('admin','SecurityController');
Routing::post('displayTeam', 'TeamController');
Routing::post('login', 'SecurityController');
Routing::post('createTeam', 'TeamController');
Routing::post('createGame', 'SpecificTeamController');
Routing::post('createGameDetails', 'SpecificTeamController');
Routing::post('changePassword','SecurityController');

Routing::post('register', 'SecurityController');
Routing::post('search', 'TeamController');
Routing::post('adminLogin','SecurityController');



Routing::run($path);