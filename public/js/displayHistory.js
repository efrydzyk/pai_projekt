const historyLinks = document.querySelectorAll(".history-viewer");

function displayHistory() {
    const history = this;
    const id =history.getAttribute("id");
    console.log(id);
    return location.href = `http://localhost:8080/displayHistory/${id}`;

}

historyLinks.forEach(button => button.addEventListener("click", displayHistory));