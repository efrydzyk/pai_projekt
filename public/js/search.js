const search = document.querySelector('input[placeholder="search team"]');
const teamContainer = document.querySelector(".teams");
const container = document.querySelector(".toGetId")
const id = container.getAttribute("id");


search.addEventListener("keyup", function(event){


    if(event.key === "Enter") {
         event.preventDefault();

        const data = {search: this.value};

        console.log(data);
            fetch("/search", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            }).then(function (response) {
                return response.json();
            }).then(function (teams) {
                teamContainer.innerHTML = "";
                loadTeams(teams)
            });
        }


});

function loadTeams(teams) {
    teams.forEach(team => {
        console.log(team);
        if(team.id_assigned_by == id) {
            createTeam(team);
        }
    });
}

function createTeam(team) {
    const template = document.querySelector("#team-template");

    const clone = template.content.cloneNode(true);
    const link = clone.querySelector("i");
    link.id = team.id;
    const logo = clone.querySelector("img");
    logo.src = `/public/uploads/${team.logo}`;
    const teamName = clone.querySelector("h2");
    teamName.innerHTML = team.team_name;
    const game = clone.querySelector("#game");
    game.innerHTML = team.game;
    const league = clone.querySelector("#league");
    league.innerHTML= team.league;

    teamContainer.appendChild(clone);

}