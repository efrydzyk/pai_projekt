<?php
session_start();

//if (!isset($_COOKIE['currentUser']) || (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] != 'admin'))
//{
//header("Location: adminLogin");
//}
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/public/css/teams.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/./public/js/display.js" defer></script>


    <title>LOGIN PAGE</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="/public/img/logo.svg">
            <?php
            if (isset($_COOKIE['currentUser'])) {
                echo '<li ><a id="logout" class ="button" href="logout">Wyloguj</a></li>';
            }
            ?>
    </nav>

    <main>
        <?php foreach ($users as $user): ?>
<div id="users-div">
        <div id="user-info">
            <div id="sett-box">
                <h1>Login: </h1>
                <p><?= $user->getLogin(); ?></p>
            </div>
            <div id="sett-box">
                <h1>Email: </h1>
                <p><?= $user->getEmail(); ?></p>
            </div>
            <div id="sett-box">
                <h1>Name: </h1>
                <p><?= $user->getName(); ?></p>
            </div>
            <div id="sett-box">
                <h1>Surname:</h1>
                <p><?= $user->getSurname(); ?></p>
            </div>
            <div id="sett-box">
                <h1>Country: </h1>
                <p><?= $user->getCountry(); ?></p>
            </div>
            <div id="sett-box">
                <h1>Phone: </h1>
                <p><?= $user->getPhone(); ?></p>
            </div>


        </div>

</div>



        <?php endforeach; ?>



    </main>


</div>
</body>
