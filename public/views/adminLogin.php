<?php
session_start();

//if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin')
//{
//    header("Location: adminPanel");
//}

?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <title>LOGIN PAGE</title>
</head>
<body>
<div class="container">
    <div class="logo">
        <img class="logotyp" src="public/img/logo.svg">

    </div>
    <div class="login-container">
        <form class="login" action="adminLogin" method="POST">
            <h1>LOGIN AS AN ADMIN</h1>
            <input name="email" type="text" placeholder="email@email.com">
            <input name="password" type="password" placeholder="password">
            <div class ="messages">
                <?php if(isset($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <button type="submit">LOGIN</button>
        </form>
    </div>

</div>
</body>