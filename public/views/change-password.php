<?php
session_start();
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/public/css/teams.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/./public/js/display.js" defer></script>


    <title>LOGIN PAGE</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="/public/img/logo.svg">
        <ul>
            <li style="display:none;">
                <a href="history">    <i class="fas fa-history"></i> </a>
                <a id="left-bar"  href="" class="button">history</a>
            </li>
            <li>
                <a href="createTeam">    <i class="fas fa-plus-circle"></i> </a>
                <a id="left-bar" href="createTeam" class="button">create team</a>
            </li>
            <li>
                <a href="teams">   <i class="fas fa-user-friends"></i> </a>
                <a id="left-bar" href="teams" class="button">teams</a>
            </li>
            <li>
                <a href="settings"> <i class="fas fa-cog"></i> </a>
                <a id="left-bar" href="settings" class="button">settings</a>
            </li>
            <?php
            if (isset($_COOKIE['currentUser'])) {
                echo '<li ><a id="logout" class ="button" href="logout">Wyloguj</a></li>';
            }
            ?>
        </ul>
    </nav>
    <main>
        <header style="display: none">
            <div class="search-bar">
                <input placeholder="search history">
            </div>
            <div class="scrim-adder">
                <i class="fas fa-plus-circle"></i>
                add scrim
            </div>
        </header>
        <section>
            <div >
                <h4>Change password</h4>
                <form action="changePassword" method="POST">
                    <input type="password" name="password" placeholder="new password"><br>
                    <input type="password" name="confirmedPassword" placeholder="repeat new password">
                    <div class="messages">
                        <?php
                        if (isset($messages))
                        {
                            foreach ($messages as $message)
                            {
                                echo "<span style='color: red;'>$message</span>";
                            }
                        }
                        ?>
                    </div>
                    <input type="submit" value="save">
                </form>
            </div>
        </section>








    </main>

</div>
</body>
