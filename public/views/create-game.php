<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/public/css/teams.css">
    <link rel="stylesheet" type="text/css" href="/public/css/creategame.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/addGameDetails.js" defer></script>

    <title>LOGIN PAGE</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="/public/img/logo.svg">
        <ul>
            <li style="display:none;">
                <a href="http://localhost:8080/history">    <i class="fas fa-history"></i> </a>
                <a id="left-bar"  href="http://localhost:8080/history" class="button">history</a>
            </li>
            <li >
                <a href="http://localhost:8080/createTeam">    <i class="fas fa-plus-circle"></i> </a>
                <a id="left-bar" href="http://localhost:8080/createTeam" class="button">create team</a>
            </li>
            <li>
                <a href="http://localhost:8080/teams">   <i class="fas fa-user-friends"></i> </a>
                <a id="left-bar" href="http://localhost:8080/teams" class="button">teams</a>
            </li>
            <li>
                <a href="http://localhost:8080/settings"> <i class="fas fa-cog"></i> </a>
                <a id="left-bar" href="http://localhost:8080/settings" class="button">settings</a>
            </li>
            <?php
            if (isset($_COOKIE['currentUser'])) {
                echo '<li ><a id="logout" class ="button" href="logout">Wyloguj</a></li>';
            }
            ?>
        </ul>
    </nav>
    <main>
        <header>
            <div class="search-bar">
                <form style="display:none;">
                    <input placeholder="search scrim">
                </form>
            </div>
            <div class="scrim-adder">
                <i class="fas fa-plus-circle"></i>
                add project
            </div>
        </header>
        <section class="game-form">
            <div id="game-creator">
                <div id="head">
            <h1> Create </h1>
                </div>
                <div id="formula">
                    <form class="allforms" action="createGame" method="POST" ENCTYPE="multipart/form-data">
                <?php if(isset($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                    }
                }
                ?>
                        <p><?= $id;?></p>
                <input name="time" type="time" placeholder="time">
                <input name="enemy_team_name" type="enemy_team_name" placeholder="enemy_team_name">
                        <input name="win" type="win" placeholder="win">
                        <input type="submit">
                        <i  id="<?= $id;?>"class="allsubmit add-game-details"type="submit">send</i>
<!--                        <button type="submit">send</button>-->

                    </form>

            </div>
        </section>

    </main>
    <script>
        $(function() {
            $(".allsubmit").click(function(){
                $('.allforms').each(function(){
                    var valuesToSend = $(this).serialize();
                    $.ajax($(this).attr('action'),
                        {
                            method: $(this).attr('method'),
                            data: valuesToSend
                        }
                    )
                });
            });
        });

    </script>
</div>
</body>