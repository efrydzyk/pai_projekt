<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="public/css/teams.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li style="display:none;">
                    <a href="history">    <i class="fas fa-history"></i> </a>
                    <a id="left-bar" href="#" class="button">history</a>
                </li>
                <li>
                    <a href="createTeam">    <i class="fas fa-plus-circle"></i> </a>
                    <a id="left-bar" href="createTeam" class="button">create team</a>
                </li>
                <li>
                    <a href="teams">   <i class="fas fa-user-friends"></i> </a>
                    <a id="left-bar" href="teams" class="button">teams</a>
                </li>
                <li>
                    <a href="settings"> <i class="fas fa-cog"></i> </a>
                    <a id="left-bar" href="settings" class="button">settings</a>
                </li>
                <?php
                if (isset($_COOKIE['currentUser'])) {
                    echo '<li ><a id="logout" class ="button" href="logout">Wyloguj</a></li>';
                }
                ?>
            </ul>
        </nav>
        <main>
            <header>
                <div style="display: none" class="search-bar">
                    <form>
                        <input placeholder="search scrim">
                    </form>
                </div>
                <div style="visibility: hidden;" class="scrim-adder">
                    <i class="fas fa-plus-circle"></i>
                    add project
                </div>
            </header>
            <section class="team-form">
                <h1 style="padding-left: 2vw;"> Create Team </h1>
                <form action="createTeam" method="POST" ENCTYPE="multipart/form-data">
                    <?php if(isset($messages)) {
                        foreach ($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                    <input name="name" type="name" placeholder="name">
                    <input name="game" type="game" placeholder="game">
                    <input name="league" type="league" placeholder="league">

                    <input type="file" name="file"><br/>
                    <button type="submit">send</button>

                </form>
                

            </section>
        </main>

    </div>
</body>