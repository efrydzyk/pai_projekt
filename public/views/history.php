<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/public/css/teams.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
<!--    <script type="text/javascript" src="/public/js/search.js" defer></script>-->
    <script type="text/javascript" src="/public/js/history.js" defer></script>
    <title>LOGIN PAGE</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="/public/img/logo.svg">
        <ul>
            <li style="display:none;">
                <a href="http://localhost:8080/history">    <i class="fas fa-history"></i> </a>
                <a id="left-bar"  href="http://localhost:8080/history" class="button">history</a>
            </li>
            <li >
                <a href="http://localhost:8080/createTeam">    <i class="fas fa-plus-circle"></i> </a>
                <a id="left-bar" href="http://localhost:8080/createTeam" class="button">create team</a>
            </li>
            <li>
                <a href="http://localhost:8080/teams">   <i class="fas fa-user-friends"></i> </a>
                <a id="left-bar" href="http://localhost:8080/teams" class="button">teams</a>
            </li>
            <li>
                <a href="http://localhost:8080/settings"> <i class="fas fa-cog"></i> </a>
                <a id="left-bar" href="http://localhost:8080/settings" class="button">settings</a>
            </li>
            <?php
            if (isset($_COOKIE['currentUser'])) {
                echo '<li ><a id="logout" class ="button" href="logout">Wyloguj</a></li>';
            }
            ?>
        </ul>
    </nav>
    <main>
<!--        <header>-->
<!--            <div class="search-bar">-->
<!--                <input placeholder="search history">-->
<!--            </div>-->
<!--            <div class="scrim-adder">-->
<!--                <i class="fas fa-plus-circle"></i>-->
<!--                add scrim-->
<!--            </div>-->
<!--        </header>-->



        <section class="games">

            <div id="history" >
                <div id="history-text">
                    <h1>History</h1>
                </div>
                <div id="matches">
                    <div>
                        <?php if($games) foreach ($games as $game): ?>


                    <div id="match-1">
                        <div id="lol".<?= $game->getWin(); ?> class="teams-row">
                            <div id="allies-info">
                                <h1><?= $game->getTeamName()?></h1>
                                <?php foreach ($gamesDetails as $gameDetails): if($game->getId() === $gameDetails->getIdGame() && $gameDetails->getEnemy() === false): ?>
                                    <div id="champions-info">
                                    <div id="champ1">
                                        <p><?= $gameDetails->getNickname()?></p>
                                        <p><?= $gameDetails->getChampion()?></p>
                                        <div id = "stats">
                                            <p><?= $gameDetails->getKills()?></p>
                                            <p>/</p>
                                            <p><?= $gameDetails->getDeaths()?></p>
                                            <p>/</p>
                                            <p><?= $gameDetails->getAssists()?></p>
                                        </div>
                                        <p><?= $gameDetails->getCreeps()?> cr.</p>
                                        <p><?= $gameDetails->getGold()?></p>
                                    </div>

                                </div>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                            <div id="enemies-info">
                                <h1><?= $game->getEnemyTeamName()?></h1>
                                <?php foreach ($gamesDetails as $gameDetails): if($game->getId() === $gameDetails->getIdGame() && $gameDetails->getEnemy() === true): ?>

                                    <div id="champions-info">
                                    <div id="champ1">
                                        <p><?= $gameDetails->getNickname()?></p>
                                        <p><?= $gameDetails->getChampion()?></p>
                                        <div id = "stats">
                                            <p><?= $gameDetails->getKills()?></p>
                                            <p>/</p>
                                            <p><?= $gameDetails->getDeaths()?></p>
                                            <p>/</p>
                                            <p><?= $gameDetails->getAssists()?></p>
                                        </div>
                                        <p><?= $gameDetails->getCreeps()?> cr.</p>
                                        <p><?= $gameDetails->getGold()?></p>
                                    </div>

                                </div>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>

                    </div>

                    </div>
                    <?php endforeach; ?>
                </div>
            </div>



        </section>





            </main>
</div>
            </body>



