<?php
session_start();

if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin')
{
    header("Location: adminPanel");
}else if (isset($_COOKIE['currentUser'])) {
    header("Location: teams");
}

?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img class="logotyp" src="public/img/logo.svg">

        </div>
        <div class="login-container">
        <form class="login" action="login" method="POST">
            <input name="email" type="text" placeholder="email@email.com">
            <input name="password" type="password" placeholder="password">
            <div class ="messages">
                <?php if(isset($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <button type="submit">LOGIN</button>
            <a style="text-decoration: none"href="register" >REGISTER</a>
            <a style="text-decoration: none; color: #707070"href="adminLogin">ADMIN</a>
        </form>
        </div>

    </div>
</body>