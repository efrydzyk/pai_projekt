<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <script type="text/javascript" src="./public/js/script.js" defer></script>
    <title>REGISTER</title>
</head>

<body>
<div class="container">
    <div class="logo">
        <img class="logotyp" src="public/img/logo.svg">

    </div>
    <div class="login-container">
        <form class="register" action="register" method="POST">
            <div class ="messages">
                <?php if(isset($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <input name="login" type="text" placeholder="login">
            <input name="email" type="text" placeholder="email@email.com">
            <input name="password" type="password" placeholder="password">
            <input name="confirmedPassword" type="password" placeholder="confirm password">
            <input name="name" type="text" placeholder="name">
            <input name="surname" type="text" placeholder="surname">
            <input name="country" type="text" placeholder="your country">
            <input name="phone" type="text" placeholder="your phone number">
            <button type="submit">REGISTER</button>

        </form>
    </div>

</div>
</body>