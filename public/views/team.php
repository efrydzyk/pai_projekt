<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/public/css/teams.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
<!--    <script type="text/javascript" src="/./public/js/display.js" defer></script>-->
    <script type="text/javascript" src="/./public/js/displayHistory.js" defer></script>
    <script type="text/javascript" src="/./public/js/addGame.js" defer></script>



    <title>LOGIN PAGE</title>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="/public/img/logo.svg">
        <ul>
            <li>
                <a href="http://localhost:8080/history">    <i class="fas fa-history"></i> </a>
                <a id="left-bar"  href="http://localhost:8080/history" class="button">history</a>
            </li>
            <li>
                <a href="http://localhost:8080/createTeam">    <i class="fas fa-plus-circle"></i> </a>
                <a id="left-bar" href="http://localhost:8080/createTeam" class="button">create team</a>
            </li>
            <li>
                <a href="http://localhost:8080/teams">   <i class="fas fa-user-friends"></i> </a>
                <a id="left-bar" href="http://localhost:8080/teams" class="button">teams</a>
            </li>
            <li>
                <a href="http://localhost:8080/settings"> <i class="fas fa-cog"></i> </a>
                <a id="left-bar" href="http://localhost:8080/settings" class="button">settings</a>
            </li>
            <?php
            if (isset($_COOKIE['currentUser'])) {
                echo '<li ><a id="logout" class ="button" href="http://localhost:8080/logout">Wyloguj</a></li>';
            }
            ?>
        </ul>
    </nav>
    <main>
        <header>
            <div class="search-bar">
                <input style="display: none" placeholder="search history">
            </div>
            <i id ="<?= $team->getId(); ?>" class="scrim-adder">
                <i  class="fas fa-plus-circle"></i>
                add scrim
            </i>
        </header>


        <section class="team">
            <?php ?>
                    <div id="team-1" >
                        <div id="left-side">
                            <h1><?= $team->getName(); ?></h1>
                            <img id="logo" src="/public/uploads/<?= $team->getImage() ?>">
                        </div>
                        <div id="right-side">
                            <div id ="additional-info">
                                <div id="add-info">
                                    <h2>Game:</h2>
                            <p><?= $team->getGame(); ?></p>
                                </div>
                                <div id="add-info">
                                    <h2>League:</h2>
                            <p><?= $team->getLeague(); ?></p>
                            </div>
                            </div>
                            <div id="players-tag">
                                <h2>Players:</h2>

                            </div>
                            <div id ="players">

                                <p>Player1</p>
                                <p>Player2</p>
                                <p>Player3</p>
                                <p>Player4</p>
                                <p>Player5</p>

                            </div>
                            <div id="match-history-button">
                                <i id="<?= $team->getId(); ?>"  class="history-viewer">Match history</i>
                            </div>
                        </div>


                    </div>







            <?php ?>


        </section>


    </main>

</div>
</body>

<template id="team-template">
    <div id="">
        <img src="">
        <div>
            <h2>teamName</h2>
            <p id="game">game</p>
            <p id="league">league</p>

        </div>
    </div></template>