
<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/public/css/teams.css">
    <script src="https://kit.fontawesome.com/2d8d12c35c.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/search.js" defer></script>
    <script type="text/javascript" src="./public/js/display.js" defer></script>

    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul><li style="display:none;">
                    <a href="history">    <i class="fas fa-history"></i> </a>
                    <a id="left-bar" href="history" class="button">history</a>
                </li>
                <li>
                    <a href="createTeam">    <i class="fas fa-plus-circle"></i> </a>
                    <a id="left-bar" href="createTeam" class="button">create team</a>
                </li>
                <li>
                    <a href="teams">   <i class="fas fa-user-friends"></i> </a>
                    <a id="left-bar" href="teams" class="button">teams</a>
                </li>
                <li>
                   <a href="settings"> <i class="fas fa-cog"></i> </a>
                    <a id="left-bar" href="settings" class="button">settings</a>
                </li>
                <?php
                if (isset($_COOKIE['currentUser'])) {
                    echo '<li ><a id="logout" class ="button" href="logout">Wyloguj</a></li>';
                }
                ?>
            </ul>
        </nav>
        <main>
            <header>
                <div class="search-bar">
                        <input placeholder="search team">
                </div>
                <div style="visibility: hidden " class="scrim-adder">
                    <i class="fas fa-plus-circle"></i>
                    add scrim
                </div>
            </header>


            <section class="teams">


                <?php foreach ($teams as $team): ?>

                <i id="<?= $team->getId(); ?>" class = "team-link" >
                <div>

                    <img src="/public/uploads/<?= $team->getImage(); ?>">
                    <div>
                        <h2><?= $team->getName(); ?></h2>
                        <p><?= $team->getGame(); ?></p>
                        <p><?= $team->getLeague(); ?></p>

                    </div>

                </div>

                </i>



                <?php endforeach; ?>


                

            </section>
        </main>

    </div>

<div style="display: none" id="<?=$user->getId(); ?>" class="toGetId"></div>
</body>

<template id="team-template">
    <i id="" class = "team-link">
        <div>
        <img src="">
        <div>
            <h2>teamName</h2>
            <p id="game">game</p>
            <p id="league">league</p>

        </div>
        </div>
    </i>
</template>

<!--<template id="team-template">-->
<!---->
<!--<div id=""  >-->
<!--    <div>-->
<!---->
<!--        <img src="">-->
<!--        <div>-->
<!--            <h2>teamName</h2>-->
<!--            <p>game</p>-->
<!--            <p>league</p>-->
<!---->
<!--        </div>-->
<!---->
<!--    </div>-->
<!--</div-->
<!--</template>>-->