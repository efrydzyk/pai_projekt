create table users_details
(
    id      serial
        constraint users_details_pk
            primary key,
    name    varchar(100) not null,
    surname varchar(100) not null,
    country varchar(100),
    phone   varchar(20)
);

alter table users_details
    owner to lnyudnbxpehghz;

create table users
(
    id_user         integer default nextval('"users_ID_user_seq"'::regclass) not null
        constraint users_pk
            primary key,
    login           varchar(100)                                             not null,
    email           varchar(255)                                             not null,
    password        varchar(255)                                             not null,
    id_user_details integer default 0                                        not null
        constraint details_users___fk
            references users_details
            on update cascade on delete cascade,
    enabled         boolean default false                                    not null,
    account_type    integer default 0                                        not null
);

alter table users
    owner to lnyudnbxpehghz;

create unique index users_id_user_uindex
    on users (id_user);

create unique index users_email_uindex
    on users (email);

create table teams
(
    id             serial
        constraint teams_pk
            primary key,
    team_name      varchar(100) not null,
    game           varchar(100) not null,
    created_at     date         not null,
    id_assigned_by integer      not null
        constraint teams_users_id_user_fk
            references users
            on update cascade on delete cascade,
    logo           varchar(255),
    league         varchar(100)
);

alter table teams
    owner to lnyudnbxpehghz;

create unique index teams_id_uindex
    on teams (id);

create table users_teams
(
    id_user integer not null
        constraint user_users_teams___fk
            references users
            on update cascade on delete cascade,
    id_team integer not null
        constraint team_users_teams___fk
            references teams
            on update cascade on delete cascade
);

alter table users_teams
    owner to lnyudnbxpehghz;

create unique index users_details_id_uindex
    on users_details (id);

create table players
(
    id       serial
        constraint players_pk
            primary key,
    id_user  integer,
    nickname varchar(100)            not null,
    server   varchar(20)             not null,
    role     varchar(100),
    added_at timestamp default now() not null
);

alter table players
    owner to lnyudnbxpehghz;

create unique index players_id_uindex
    on players (id);

create unique index players_id_user_uindex
    on players (id_user);

create unique index players_nickname_uindex
    on players (nickname);

create table games
(
    id              serial
        constraint games_pk
            primary key,
    id_team         integer default 1 not null
        constraint games_teams_id_fk
            references teams
            on update cascade on delete cascade,
    time            time              not null,
    enemy_team_name varchar(100),
    win             boolean           not null,
    created_at      date              not null
);

alter table games
    owner to lnyudnbxpehghz;

create unique index games_id_uindex
    on games (id);

create table gamedetails
(
    id           integer   default nextval('teammates_id_seq'::regclass) not null
        constraint teammates_pk
            primary key,
    champion     varchar(100)                                            not null,
    enemy        boolean   default false,
    kills        integer                                                 not null,
    deaths       integer                                                 not null,
    assists      integer                                                 not null,
    creeps       integer                                                 not null,
    vision_score integer,
    gold         integer                                                 not null,
    created_at   timestamp default now()                                 not null,
    nickname     varchar(100)                                            not null,
    id_game      integer                                                 not null
        constraint gamedetails_games_id_fk
            references games
            on update cascade on delete cascade
);

alter table gamedetails
    owner to lnyudnbxpehghz;

create unique index teammates_id_uindex
    on gamedetails (id);


