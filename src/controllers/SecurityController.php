<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

session_start();

class SecurityController extends AppController
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }
    public function login(){


        if(!$this->isPost()){
            return $this->render('login');
        }
        $email = $_POST["email"];
        $password = $_POST["password"];

        $user = $this->userRepository->getUser($email);

        if(!$user){
            return $this->render('login', ['messages' => ['User not exists']]);
        }

        if($user->getEmail() !== $email){
            return $this->render('login', ['messages' => ['User with this email not exists']]);

        }

        if(!(password_verify($password, $user->getPassword()))){
            return $this->render('login', ['messages' => ['wrong password']]);
        }
        $this->userRepository->setCookieUser($user->getEmail());


        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/teams");

    }

    public function settings()
    {
    $user=$this->userRepository->getUserInfo();
        $this->render('settings', ['user' => $user]);

    }
    public function logout()
    {
        $this->userRepository->deleteCookieUser();

//        $url = "https://$_SERVER[HTTP_HOST]";
        header("Location: login");

    }
    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }
        $login = $_POST['login'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $country = $_POST['country'];
        $phone = $_POST['phone'];

        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }

        //TODO try to use better hash function
        $user = new User($login, $email, password_hash($password, PASSWORD_BCRYPT), $name, $surname, $country, $phone);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }

    public function changePassword()
    {
        if (!$this->isPost())
        {
            return $this->render('change-password');

            $this->userRepository->getUser($_COOKIE['currentUser']);
        }

        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];

        if ($password !== $confirmedPassword)
        {
            return $this->render('change-password', ['messages' => ['Wprowadź poprawne hasło!']]);
        } else {
            $this->userRepository->updateProfilePassword(password_hash($password, PASSWORD_BCRYPT));

            $this->render('change-password', ['messages' => ['Hasło zostało zaktualizowane.']]);
        }

    }


    //////////ADMIN
    ///
    ///

    public function adminLogin()
    {
        if (!$this->isPost())
        {
            return $this->render('adminLogin');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];

        $user = $this->userRepository->getUser($email);

        $userType = $user->getAccountType();

        if ($userType !== 1)
        {
            return $this->render('adminLogin', ['messages' => ['not an administrator'.$userType]]);
        }

        if ($user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['user does not exist!']]);
        }

        if (!(password_verify($password, $user->getPassword()))) {
            return $this->render('adminLogin', ['messages' => ['Wrong password!']]);
        }

        $this->userRepository->setCookieUser($user->getEmail());


       // $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: http://localhost:8080/admin");

    }

    public function admin()
    {

        $users=$this->userRepository->getUsers();
        $this->render('admin', ['users' => $users]);

    }

}
