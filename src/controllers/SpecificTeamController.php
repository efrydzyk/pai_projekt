<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Team.php';
require_once __DIR__.'/../models/Game.php';
require_once __DIR__.'/../models/GameDetails.php';
require_once __DIR__.'/../repository/TeamRepository.php';
require_once __DIR__.'/../repository/GameRepository.php';



class SpecificTeamController extends AppController
{

    private bool $isEnemy = true;
    private bool $isWin = true;
    private $messages = [];
    private $teamRepository;
    private $gameRepository;


    public function __construct()
    {
        parent::__construct();
        $this->teamRepository = new TeamRepository();
        $this->gameRepository = new GameRepository();
    }


    public function displayHistory(int $id){
        $games = $this->gameRepository->displayHistory($id);
        $gamesDetails = $this->gameRepository->displayGameDetails($id);

        $this->render('history', [
          'games' => $games,
          'gamesDetails' => $gamesDetails
      ]);


    }





    public function createGame($id)
    {
        $team = $this->teamRepository->displayTeam($id);
            if($this->isPost()) {

                if($_POST['win'] === "false"){
                    $isWin = false;
                }
                $game = new Game($_POST['time'], $_POST['enemy_team_name'], $isWin);

                echo var_dump($game);
//                die();
               $this->gameRepository->addGame($id, $game);

                return $this->render('add-game-details');
          //      return $this->render('teams');
            }
        return $this->render('create-game', ['id' => $id]);
    }

    public function createGameDetails(int $id)
    {

        if($this->isPost()) {

            if($_POST['enemy'] === "false"){
                $isEnemy = false;
            }
            $gameDetails = new GameDetails($_POST['champion'], $isEnemy, $_POST['kills'], $_POST['deaths'], $_POST['assists'], $_POST['creeps'], $_POST['vision_score'], $_POST['gold'], $_POST['nickname']);
            $this->gameRepository->addGameDetails($game, $gameDetails);
            $games = $this->gameRepository->displayHistory($id);
            $gamesDetails = $this->gameRepository->displayGameDetails($id);
            return $this->render('history', [
                'games' => $games,
                'gamesDetails' => $gamesDetails
            ]);
        }
        return $this->render('add-game-details', ['messages' => $this->messages]);
    }


}