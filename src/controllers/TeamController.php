<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Team.php';
require_once __DIR__.'/../repository/TeamRepository.php';
require_once __DIR__.'/../repository/UserRepository.php';



class TeamController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private $teamRepository;
    private $userRepository;


    public function __construct()
    {
       parent::__construct();
       $this->teamRepository = new TeamRepository();
       $this->userRepository = new UserRepository();
    }

    public function teams(){
        // TODO display teams.html
        $user = $this->userRepository->getUserInfo();
        $teams = $this->teamRepository->getTeams();
        $this->render('teams', ['teams' => $teams, 'user' => $user]);
    }

    public function createTeam()
    {
        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])){
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $team = new Team($_POST['name'], $_POST['game'], $_FILES['file']['name'], $_POST['league']);
            $this->teamRepository->addTeam($team);

            return $this->render('teams', [
                'messages' => $this->messages,
                'teams' => $this->teamRepository->getTeams()
            ]);
        }
        return $this->render('create-team', ['messages' => $this->messages]);
    }

    public function search()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '' ;
        if($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-Type: application/json');
            http_response_code(200);
            $value = $this->teamRepository->getTeamByName($decoded['search']);
            echo json_encode($value);
        } else echo("DUPA DUPA");

    }


    private function validate(array $file): bool
    {
        if($file['size'] > self::MAX_FILE_SIZE) {
            $this->messages[] = 'File is too large for destination file system';
            return false;

        }

        if(!isset($file['type']) && !in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'File type not supported';
            return false;
        }
        return true;
    }

    public function displayTeam(int $id){
      $team = $this->teamRepository->displayTeam($id);

//    $this->render('team', ['team' => $this->teamRepository->displayTeam($id)]);
        $this->render('team', ['team' => $team]);
        http_response_code(212);
    }
}