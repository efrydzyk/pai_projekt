<?php

class Game
{

    private $id;
    private $id_team;
    private $time;
    private $enemy_team_name;
    private $win;
    private $team_name;

    public function getTeamName(): string
    {
        return $this->team_name;
    }

    public function setTeamName(string $team_name)
    {
        $this->team_name = $team_name;
    }


    public function __construct( $team_name = null,$id_team, $time, $enemy_team_name, $win, $id = null)
    {
        $this->team_name= $team_name;
        $this->id_team = $id_team;
        $this->time = $time;
        $this->enemy_team_name = $enemy_team_name;
        $this->win = $win;
        $this->id= $id;

    }

    public function getIdTeam() : int
    {
        return $this->id_team;
    }

    public function setIdTeam(int $id_team)
    {
        $this->id_team = $id_team;
    }

    public function getTime() : string
    {
        return $this->time;
    }

    public function setTime(string $time)
    {
        $this->time = $time;
    }

    public function getEnemyTeamName() : string
    {
        return $this->enemy_team_name;
    }

    public function setEnemyTeamName(string $enemy_team_name)
    {
        $this->enemy_team_name = $enemy_team_name;
    }


    public function getWin() : bool
    {
        return $this->win;
    }

    public function setWin(bool $win)
    {
        $this->win = $win;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }


}