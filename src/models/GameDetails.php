<?php

class GameDetails
{
    private $id;
    private $champion;
    private $enemy;
    private $kills;
    private $deaths;
    private $assists;
    private $creeps;
    private $vision_score;
    private $gold;
    private $nickname;
    private $id_game;




    public function __construct( $champion, $enemy, $kills, $deaths, $assists, $creeps, $vision_score, $gold, $nickname, $id = null, $id_game = null)
    {



        $this->champion = $champion;
        $this->enemy = $enemy;
        $this->kills = $kills;
        $this->deaths = $deaths;
        $this->assists = $assists;
        $this->creeps = $creeps;
        $this->vision_score = $vision_score;
        $this->gold = $gold;
        $this->nickname = $nickname;
        $this->id = $id;
        $this->id_game = $id_game;
    }

    public function getEnemy()
    {
        return $this->enemy;
    }

    public function setEnemy($enemy): void
    {
        $this->enemy = $enemy;
    }
    public function getIdGame(): int
    {
        return $this->id_game;
    }

    public function setIdGame(int $id_game)
    {
        $this->id_game = $id_game;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }


    public function getChampion() : string
    {
        return $this->champion;
    }

    public function setChampion(string $champion)
    {
        $this->champion = $champion;
    }


    public function getKills(): int
    {
        return $this->kills;
    }

    public function setKills(int$kills)
    {
        $this->kills = $kills;
    }

    public function getDeaths(): int
    {
        return $this->deaths;
    }

    public function setDeaths(int $deaths)
    {
        $this->deaths = $deaths;
    }

    public function getAssists() : int
    {
        return $this->assists;
    }

    public function setAssists(int $assists)
    {
        $this->assists = $assists;
    }

    public function getCreeps() : int
    {
        return $this->creeps;
    }

    public function setCreeps(int $creeps)
    {
        $this->creeps = $creeps;
    }

    public function getVisionScore() : int
    {
        return $this->vision_score;
    }

    public function setVisionScore(int $vision_score)
    {
        $this->vision_score = $vision_score;
    }

    public function getGold() : int
    {
        return $this->gold;
    }

    public function setGold(int $gold)
    {
        $this->gold = $gold;
    }

    public function getNickname() : string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname)
    {
        $this->nickname = $nickname;
    }



}