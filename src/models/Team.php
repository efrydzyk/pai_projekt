<?php

class Team
{
    private $name;
    private $game;
    private $image;
    private $league;
    private $id;

    public function __construct($name, $game,$image, $league, $id=null)
    {
        $this->name = $name;
        $this->game = $game;
        $this->league = $league;
        $this->image = $image;
        $this->id = $id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getGame() : string
    {
        return $this->game;
    }

    public function setGame(string $game)
    {
        $this->game = $game;
    }


    public function getLeague() : string
    {
        return $this->league;
    }

    public function setLeague(string $league)
    {
        $this->league = $league;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }



}