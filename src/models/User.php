<?php

class User
{
    private $email;
    private $login;
    private $password;


    private $name;
    private $surname;
    private $country;
    private $phone;
    private $account_type;


    public function __construct( $login,$email, $password = null, $name, $surname, $country = null, $phone = null, $account_type=null)
    {

        $this->login = $login;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->country = $country;
        $this->phone = $phone;
        $this->account_type = $account_type;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login): void
    {
        $this->login = $login;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }
    public function setId($id): void
    {
        $this->id_user = $id;
    }

    public function getId()
    {
        return $this->id_user;
    }

    public function getAccountType(): int
    {
        return $this->account_type;
    }

    public function setAccountType(int $account_type)
    {
        $this->account_type = $account_type;
    }
}