<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Game.php';
require_once __DIR__.'/../models/GameDetails.php';



class GameRepository extends Repository
{
    public function getGame(int $id): ?Game
    {
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM games WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $game = $stmt->fetch(PDO::FETCH_ASSOC);

        if($game == false) {
            return null;
        }
        return new Game(
            $game['team_name'],
            $game['id_team'],
            $game['time'],
            $game['enemy_team_name'],
            $game['win'],
            $game['id'],

        );
    }

    public function addGame(int $id, Game $game)
    {
        $isTrue;
        if($game->getWin() == false){$isTrue =0;}else{$isTrue =1;}
        $date = new DateTime();
        $stmt = $this->database->connect()->prepare('
        INSERT INTO games (id_team, time, enemy_team_name, win, created_at) 
        VALUES (?, ?, ?, ?, ?)');
//        echo var_dump($id,
//            $game->getTime(),S
//            $game->getEnemyTeamName(),
//            $game->getWin(),
//            $date->format('Y-m-d'));

//        die();
        $stmt ->execute([
            $id,
            $game->getTime(),
            $game->getEnemyTeamName(),
            $isTrue,
           $date->format('Y-m-d')
        ]);

      // return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function addGameDetails(Game $game, GameDetails $gameDetails)
    {

        $date = new DateTime();
        $stmt = $this->database->connect()->prepare('
        INSERT INTO gamedetails (champion, enemy, kills, deaths, assists, creeps, vision_score, gold, created_at, nickname, id_game) 
        VALUES (?, ?, ?, ?, ?,?,?,?,?,?,?)');

        $stmt ->execute([
            $gameDetails->getChampion(),
            $gameDetails->getEnemy(),
            $gameDetails->getKills(),
            $gameDetails->getDeaths(),
            $gameDetails->getAssists(),
            $gameDetails->getCreeps(),
            $gameDetails->getVisionScore(),
            $gameDetails->getGold(),
            $date->format('Y-m-d'),
            $gameDetails->getNickname(),
            $game->getId()
        ]);
    }



    public function displayHistory(int $idTeam): ?array
    {

        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT g.id, id_team, "time", enemy_team_name, win, team_name  FROM games g LEFT JOIN teams t ON g.id_team = t.id WHERE t.id = :idTeam');

        $stmt->bindParam(':idTeam', $idTeam, PDO::PARAM_INT);
        $stmt->execute();

        $games = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($games == false) {
            return null;
        }
        foreach ($games as $game){
            $result[] = new Game(
                $game['team_name'],
                $game['id_team'],
                $game['time'],
                $game['enemy_team_name'],
                $game['win'],
                $game['id'],

            );
        }
        return $result;

    }

    public function displayGameDetails(int $id): ?array
    {
        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM gamedetails gd LEFT JOIN games g ON gd.id_game = g.id WHERE g.id_team = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $gamesDetails = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($gamesDetails == false) {
            return null;
        }

        foreach ($gamesDetails as $gameDetails) {
            $result[] = new GameDetails(
                $gameDetails['champion'],
                $gameDetails['enemy'],
                $gameDetails['kills'],
                $gameDetails['deaths'],
                $gameDetails['assists'],
                $gameDetails['creeps'],
                $gameDetails['vision_score'],
                $gameDetails['gold'],
                $gameDetails['nickname'],
                $gamesDetails['id'],
                $gameDetails['id_game']

            );
        }
        return $result;

    }

//    public function addGameDetails(GameDetails $gameDetails): void
//    {
//        $date = new DateTime();
//        $stmt = $this->database->connect()->prepare('
//        INSERT INTO gamedetails (champion, kills, deaths, assists, creeps, vision_score, gold,  created_at, nickname, id_game)
//        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);');
//
//        $stmt ->execute([
//            $gameDetails->getChampion(),
//            $gameDetails->getKills(),
//            $gameDetails->getDeaths(),
//            $gameDetails->getAssists(),
//            $gameDetails->getCreeps(),
//            $gameDetails->getVisionScore(),
//            $gameDetails->getGold(),
//            $date->format('Y-m-d'),
//            $gameDetails->getNickname(),
//        ]);
//}

//    public function connect(Game $game) : array {
//        if($gamesDetails != null) {
//            $players = [];
//            foreach ($gamesDetails as $gameDetails) {
//
//                if ($game->getId() == $gameDetails->getIdGame()) {
//                    $players[] = new GameDetails(
//                        $gameDetails['champion'],
//                        $gameDetails['enemy'],
//                        $gameDetails['kills'],
//                        $gameDetails['deaths'],
//                        $gameDetails['assists'],
//                        $gameDetails['creeps'],
//                        $gameDetails['vision_score'],
//                        $gameDetails['gold'],
//                        $gameDetails['nickname'],
//                        $gameDetails['id_game']
//                    );
//                }
//            }
//        }
//        return $players;
//    }


}