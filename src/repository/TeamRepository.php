<?php

require_once 'Repository.php';
require_once 'UserRepository.php';
require_once __DIR__.'/../models/Team.php';

class TeamRepository extends Repository
{

    public function getTeam(int $id): ?Team
    {
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM teams WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $team = $stmt->fetch(PDO::FETCH_ASSOC);

        if($team == false) {
            return null;
        }
        return new Team(
            $team['team_name'],
            $team['game'],
            $team['league'],
            $team['logo'],
            $team['id']

        );
    }

    public function addTeam(Team $team): void
    {
        $stmt = $this->database->connect()->prepare('
        SELECT id_user FROM users WHERE email = :email
        ');

        $stmt->bindParam(':email', $_COOKIE['currentUser'], PDO::PARAM_STR);

        $stmt->execute();
        $idUser= $stmt->fetch(PDO::FETCH_ASSOC);
        $date = new DateTime();
        $id = $idUser['id_user'];
        $stmt = $this->database->connect()->prepare('
        INSERT INTO teams (team_name, game, created_at, id_assigned_by, logo, league) 
        VALUES (?, ?, ?, ?, ?, ?)');


        $stmt ->execute([
            $team->getName(),
            $team->getGame(),
            $date->format('Y-m-d'),
            $id,
            $team->getImage(),
            $team->getLeague()
        ]);
    }

    public function getTeams(): array
    {

        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT id_user FROM users WHERE email = :email
        ');

        $stmt->bindParam(':email', $_COOKIE['currentUser'], PDO::PARAM_STR);

        $stmt->execute();
        $idUser= $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
        SELECT * FROM teams WHERE id_assigned_by = :idUser
        ');

        $stmt->bindParam(':idUser', $idUser['id_user'], PDO::PARAM_INT);
        $stmt->execute();

        $teams = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($teams as $team){
            $result[] = new Team(
                $team['team_name'],
                $team['game'],
                $team['logo'],
                $team['league'],
                $team['id']
            );
        }
        return $result;

    }

    public function getTeamByName(string $searchString) :array
    {
        $searchString = '%'.strtolower($searchString).'%';


        $stmt = $this->database->connect()->prepare('
        SELECT t.id, t.team_name, t.game,t.id_assigned_by, t.logo, t.league FROM teams t WHERE  LOWER(team_name) LIKE :search OR LOWER(game) LIKE :search OR LOWER(league) LIKE :search
        ');
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        $teams = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $teams;


    }

    public function displayTeam(int $id){
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM teams WHERE id = :id');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $team = $stmt->fetch(PDO::FETCH_ASSOC);

        if($team == false) {
            return null;
        }
        return new Team(
            $team['team_name'],
            $team['game'],
            $team['logo'],
            $team['league'],
            $team['id']

        );
    }
}