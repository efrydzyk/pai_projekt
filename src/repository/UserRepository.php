<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

session_start();

class UserRepository extends Repository
{

//    public function getUser(string $email): ?User
//    {
//        $stmt = $this->database->connect()->prepare('
//        SELECT * FROM public.users WHERE email = :email
//        ');
//        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
//        $stmt->execute();
//
//        $user = $stmt->fetch(PDO::FETCH_ASSOC);
//
//        if($user == false) {
//            return null;
//        }
//        return new User(
//            $user['email'],
//            $user['password'],
//            $user['name'],
//            $user['surname']
//        );
//    }


    public function getUsers(): array
    {
        $newUsers = [];

        $stmt = $this->database->connect()->prepare('SELECT * FROM users u LEFT JOIN users_details ud 
            ON u.id_user_details = ud.id');
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user)
        {
            $newUsers[] = new User(
                $user['login'],
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname'],
                $user['country'],
                $user['phone'],
                $user['account_type']

            );

        }

        return $newUsers;
    }

    public function getUser(string $email): ?User
    {
//        $stmt = $this->database->connect()->prepare('
//            SELECT t2.name, t2.surname, t2.country, t2.phone,  t2.login, t2.email, t2.password, t2.id_user
//            FROM
//                 (SELECT t1.name, t1.surname, t1.country, t1.phone, t1.login, t1.email, t1.password, t1.id_user
//            FROM
//                 (SELECT name, surname, country, phone, login, email, password, id_user
//            FROM users u
//                LEFT JOIN users_details ud ON ud.id = u.id_user_details
//            WHERE email = :email) t1
//        ');

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users u LEFT JOIN users_details ud 
            ON u.id_user_details = ud.id WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false)
        {
            return null;
        }

        $newUser = new User(
            $user['login'],
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['country'],
            $user['phone'],
            $user['account_type']
        );
        $newUser->setId($user['id_user']);
//        $_SESSION['name'] = $user['name'];
//        $_SESSION['surname'] = $user['surname'];
//        $_SESSION['email'] = $user['email'];
//        $_SESSION['phon_number'] = $user['phon_number'];
//        $_SESSION['street'] = $user['street'];
//        $_SESSION['street_number'] = $user['street_number'];
//        $_SESSION['postal_code'] = $user['postal_code'];
//        $_SESSION['city'] = $user['city'];
//        $_SESSION['profile_picture'] = $user['profile_picture'];

        return $newUser;
    }

    public function addUser(User $user)
    {
        $stmt = $this->database->connect()->prepare('            
        WITH identity AS (INSERT INTO users_details (name, surname, country, phone) VALUES (?, ?, ?, ?) 
        RETURNING "id") 
        INSERT INTO users (login, email, password, "id_user_details") 
        VALUES (?, ?, ?, (SELECT "id" FROM identity))

        ');

        $stmt->execute([
            $user->getName(),
            $user->getSurname(),
            $user->getCountry(),
            $user->getPhone(),
            $user->getLogin(),
            $user->getEmail(),
            $user->getPassword()
        ]);

        $stmt = $this->database->connect()->prepare('
                    SELECT "id_user" FROM users u INNER JOIN users_details ud ON ud."id" = 
                        u."id_user_details" WHERE 
                                                  name = :name 
                                              AND surname =:surname 
                                              AND country = :country
                                              AND phone = :phone 
                                              

        ');
        $name = $user->getName();
        $surname = $user->getSurname();
        $country = $user->getCountry();
        $phone = $user->getPhone();
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->bindParam(':country', $country, PDO::PARAM_STR);
        $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);
        $stmt->execute();


    }

    public function setCookieUser(string $email)
    {
        setcookie('currentUser', $email);

        $stmt = $this->database->connect()->prepare('
            UPDATE users SET enabled = true WHERE email = ?
        ');
        $stmt->execute([$email]);

        $stmt = $this->database->connect()->prepare('
            SELECT name FROM users u INNER JOIN users_details ud ON u."id_user_details" = ud."id" WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $name = $stmt->fetch(PDO::FETCH_ASSOC);

        $_SESSION['name'] = $name['name'];
    }

    public function deleteCookieUser()
    {
        setcookie('currentUser', $_COOKIE['currentUser'], time() - 1);

        $stmt = $this->database->connect()->prepare('
            UPDATE users SET enabled = false WHERE email = ?
        ');
        $stmt->execute([$_COOKIE['currentUser']]);
        session_destroy();
    }

    public function getUserInfo()
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM users u LEFT JOIN users_details ud 
            ON u.id_user_details = ud.id WHERE email = :email
        ');
        $stmt->bindParam(':email', $_COOKIE['currentUser'], PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);


        $newUser = new User(
            $user['login'],
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['country'],
            $user['phone']
        );
        $newUser->setId($user['id_user']);

        return $newUser;


    }

    public function updateProfilePassword(string $password)
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE users SET password = :password WHERE email = :email
        ');

        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':email', $_COOKIE['currentUser'], PDO::PARAM_STR);
        $stmt->execute();
    }

}